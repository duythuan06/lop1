<?php
?>
@extends('layouts.app');
@section('content')
<div class="panel-body">


    <form action="{{url('lop')}}" method="post" class="form-horizontal">
        {{csrf_field()}}
        <div class="form-group">
            <label for="lop" class="col-sm-3 control-label"> Dien lop </label>
            <div class="col-sm-6">
                <input type="text" name="lop" id="lop" class="form-control">
                <br>
                dien body
                <br>
                <input type="text" name="body" id="body" class="form-control">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <button type="submit" class="btn btn-default">
                    <i class="fa fa-plus"></i> Add lop </button></div>
        </div>
    </form>
    @if(count($lops)>0)
    <div class="panel panel0default">
        <div class="panel-heading"> Danh sach lop</div>
        <div class="panel-body">
            <table class="panel-body">
                <table class="table table-striped task-table">
                    <thead>

                        <td>&nbsp;</td>
                    </thead>
                    <tbody>
                        @foreach($lops as $lop)
                        <tr>
                            <td class="table-text">
                                <div>{{$lop->lop}} {{$lop->body}}</div>

                            </td>
                            <td>
                                <form action="/lop/{{$lop->id}}" method="post">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}
                                    <button> delete lop</button>
                                    <input type="hidden"name="method" value="DELETE">
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
        </div>
        @endif
        @endsection