<?php
use App\Lop;
use Illuminate\Validation\Validator;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/','lop@showWelcome');

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    $lops = Lop::orderBy('created_at','desc')->get();
    return view('lops',['lops'=>$lops]);
});

Route::delete('/lop/{lop}', function ($id) {
   Lop::findOrFail($id)->delete();
   return redirect('/');
});


// Route::get('/', function () {
    
//     return view('lops',['lops'=>$lops]);
// });

Route::post('/lop', function (Request $request) {
    
    $l= new Lop;
    $l->lop=$request->input('lop');
    $l->body=$request->input('body');
    $l->save();
    return redirect('/');
}); 


Route::get('hello/1', function () {
    return view('xin chao 1s');
});
Route::get('/insert', function () {
   DB::insert('insert into lops(lop,body)value(?,?)',['lop1','tieng anh']);
   return 'Done';
});
Route::get('/read', function () {
  $result = DB::select('select * from lops where id = ? ',[2]);
    return $result;
 });
 
 Route::get('/update', function () {
    $update = DB::select('update lops set lop = "New Title hihi " where id > ? ',[2]);
      return $update;
   });

   Route::get('/delete', function () {
    $delete = DB::delete('delete from lops where id = ? ',[3]);
      return $delete;
   });
   Route::get('readAll', function () {
    $lops = Lop::all();
      foreach ($lops as $p){
          echo $p->lop." ".$p->body;
          echo"<br>";
      }
   });
   Route::get('findId', function () {
    $lops = Lop::where ('id',2)
    ->orderBy('id','desc')
    ->take(1)
    ->get();
      foreach ($lops as $p){
          echo $p->lop." ".$p->body;
          echo"<br>";
      }
   });
  
   Route::get('insertORM', function () {
    $l= new Lop;
    $l->lop='lop3';
    $l->body = 'tieng anh';
    $l->save();
      }
   );
  
   Route::get('updateORM', function () {
    $l = Lop::where('id',2)->first();
    $l->lop='lop 5';
    $l->body='tieng anh';
    $l->save();
      }
   );

   Route::get('deleteORM', function () {
   Lop::where('id','>=',5)
   ->delete();
      }
   );
   Route::get('destroyORM', function () {
    Lop::destroy([6,8]);
       }
    );
